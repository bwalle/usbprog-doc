<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version='1.0'
                xmlns="http://www.w3.org/TR/xhtml1/transitional"
                exclude-result-prefixes="#default">

<xsl:import href="http://docbook.sourceforge.net/release/xsl/current/html/chunk.xsl"/>

<!-- Use stylesheet -->
<xsl:param name="html.stylesheet" select="'usbprog.css'"/>

<!-- labels and numbering -->
<xsl:param name="autotoc.label.separator" select="'. '"/>
<xsl:param name="chapter.autolabel" select="1"/>
<xsl:param name="section.autolabel" select="1"/>
<xsl:param name="section.label.includes.component.label" select="1"/>

<!-- we want image scaling only for PDF -->
<xsl:param name="ignore.image.scaling" select="1" />

<!-- Don't force the use of index.html as root filename -->
<xsl:param name="root.filename" select="''"/>

<!--  Use element id (if present) as file name  -->
<xsl:variable name="use.id.as.filename">1</xsl:variable>

<xsl:template match="releaseinfo" mode="titlepage.mode">
  <span class="{name(.)}">
    <br/>
    <xsl:apply-templates mode="titlepage.mode"/>
    <br/>
  </span>
</xsl:template>

<!-- Use graphics in admonitions (note, warning, etc)  -->
<xsl:variable name="admon.graphics">0</xsl:variable>

<xsl:param name="admon.style">
	<xsl:text>text-align: left;</xsl:text></xsl:param>

<xsl:variable name="admon.graphics.path">stylesheet-images/</xsl:variable>

<xsl:variable name="admon.graphics.extension">.png</xsl:variable>

<xsl:param name="table.border.thickness" select="'0.2pt'"/>

<xsl:param name="graphic.default.extension" select="png"/>


</xsl:stylesheet>
